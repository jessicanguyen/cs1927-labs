// testList.c - testing List data type
// Written by John Shepherd, March 2013

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "List.h"

void testListBefore();
void testListAfter();
void testListDelete();
void runUnitTests();

int main(int argc, char *argv[]) {	
	
	runUnitTests();

	return EXIT_SUCCESS;
}

void runUnitTests() {

	testListBefore();
	testListAfter();
	testListDelete();
	printf("\nAll unit tests passed!\n");
}

// Tests ListDelete() which given a list, deletes current item 
// new item becomes item following current
// if current was last, node preceding current becomes new last
// if current was the only item, current becomes null
void testListDelete() {

	printf("\nTesting ListDelete()...\n");

	List l = newList();
	printf(">> Testing an one item list \n");
	Item firstItem = "First Item";
	ListAfter(l, firstItem);
	printf("Successfully added first item!\n");
	printf("Deleting...\n");
	ListDelete(l);
	showList(stdout,l);
	assert(ListLength(l) == 0);
	printf("If nothing was printed out: test passed successfully!\n");

	printf(">> Testing the last node \n");
	Item secondItem = "Second Item";
	ListAfter(l, firstItem);
	assert(ListLength(l) == 1);
	ListAfter(l, secondItem);
	assert(ListLength(l) == 2);
	printf("Successfully added first and second item!\n");
	printf("Showing current list:\n");
	showList(stdout,l);
	printf("Deleting second item...\n");
	ListDelete(l);
	printf("Showing current list:\n");
	showList(stdout,l);
	assert(ListLength(l) == 1);
	assert(ItemEQ(firstItem, ListCurrent(l)));
	printf("Test passed successfully!\n");


	printf(">> Testing the middle node \n");
	Item thirdItem = "Third Item";
	printf("Adding second item (again) and then third item...\n");
	ListAfter(l, secondItem);
	assert(ListLength(l) == 2);
	ListAfter(l, thirdItem);
	assert(ListLength(l) == 3);
	printf("Successfully added second item and third item!\n");
	printf("Showing current list:\n");
	showList(stdout,l);
	printf("Deleting third item...\n");
	ListDelete(l);
	printf("Showing current list:\n");
	showList(stdout,l);
	assert(ListLength(l) == 2);
	assert(ItemEQ(secondItem, ListCurrent(l)));
	printf("Test passed successfully!\n");

}

// Tests ListAfter() which given a list, and a item value
// adds a new node with that item value after current node
// new item becomes current item
void testListAfter() {

	printf("\nTesting ListAfter()...\n");

	List l = newList();
	
	//Testing an empty list
	printf(">> Testing an empty list \n");
	Item firstItem = "First Item";
	ListAfter(l, firstItem);
	assert(ItemEQ(firstItem, ListCurrent(l)));
	assert(ListLength(l) == 1);
	assert(validList(l));
	printf("Test passed successfully!\n");

	//Testing current node as first node in the list
	printf(">> Testing the first node\n");
	Item secondItem = "Second Item";
	ListMoveTo(l, 1);
	ListAfter(l, secondItem);
	ListMoveTo(l, 2);
	showList(stdout, l);
	assert(ItemEQ(secondItem, ListCurrent(l)));
	assert(ListLength(l) == 2);
	assert(validList(l));
	printf("Test passed successfully!\n");

	//Testing current node as last node in the list
	printf(">> Testing the last node..\n");
	Item thirdItem = "Third Item";
	//move curr to last node
	ListMoveTo(l, ListLength(l));
	ListAfter(l, thirdItem);
	ListMoveTo(l, ListLength(l));
	assert(ItemEQ(thirdItem, ListCurrent(l)));
	assert(validList(l));
	printf("Test passed successfully!\n");

	//Testing current node as the middle node in the list
	printf(">> Testing the current node as the middle node in the list\n"); 
	Item fourthItem = "Fourth Item";
	//move curr to middle node
	ListMoveTo(l, 2);
	ListAfter(l, fourthItem);
	ListMoveTo(l, 3);
	//showList(stdout, l);
	assert(ItemEQ(fourthItem, ListCurrent(l)));
	ListMoveTo(l, ListLength(l));
	assert(validList(l));
	printf("Test passed successfully!\n");

}

// Tests ListBefore() which given a list, and a item value
// adds a new node with that item value before current node
// new item becomes current item
void testListBefore() {

	printf("\nTesting ListBefore()...\n");

	List l = newList();
	
	//Testing an empty list
	printf(">> Testing an empty list..\n");
	Item firstItem = "First Item";
	ListBefore(l, firstItem);
	assert(ItemEQ(firstItem, ListCurrent(l)));
	assert(ListLength(l) == 1);
	assert(validList(l));
	printf("Test passed successfully!\n");

	//Testing current node as first node in the list
	printf(">> Testing the first node..\n");
	Item secondItem = "Second Item";
	ListMoveTo(l, 1);
	ListBefore(l, secondItem);
	ListMoveTo(l, 1);
	showList(stdout, l);
	assert(ItemEQ(secondItem, ListCurrent(l)));
	assert(ListLength(l) == 2);
	assert(validList(l));
	printf("Test passed successfully!\n");

	//Testing current node as last node in the list
	printf(">> Testing the last node..\n");
	Item thirdItem = "Third Item";
	//move curr to last node
	ListMoveTo(l, ListLength(l));
	ListBefore(l, thirdItem);
	ListMoveTo(l, ListLength(l)-1);
	assert(ItemEQ(thirdItem, ListCurrent(l)));
	assert(validList(l));
	printf("Test passed successfully!\n");

	//Testing current node as the middle node in the list
	printf(">> Testing the current node as the middle node in the list\n"); 
	Item fourthItem = "Fourth Item";
	//move curr to middle node
	ListMoveTo(l, 2);
	ListBefore(l, fourthItem);
	ListMoveTo(l, 2);
	//showList(stdout, l);
	assert(ItemEQ(fourthItem, ListCurrent(l)));
	ListMoveTo(l, ListLength(l));
	assert(validList(l));
	printf("Test passed successfully!\n");

}