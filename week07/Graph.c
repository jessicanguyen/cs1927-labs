// Graph.c ... implementation of Graph ADT
// Written by John Shepherd, May 2013

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "Graph.h"
#include "Queue.h"

// graph representation (adjacency matrix)
typedef struct GraphRep {
	int    nV;    // #vertices
	int    nE;    // #edges
	int  **edges; // matrix of weights (0 == no edge)
} GraphRep;

// check validity of Vertex
int validV(Graph g, Vertex v)
{
	return (g != NULL && v >= 0 && v < g->nV);
}

// make an edge
Edge mkEdge(Graph g, Vertex v, Vertex w)
{
	assert(g != NULL && validV(g,v) && validV(g,w));
	Edge new = {v,w}; // struct assignment
	return new;
}

// insert an Edge
// - sets (v,w) and (w,v)
void insertEdge(Graph g, Vertex v, Vertex w, int wt)
{
	assert(g != NULL && validV(g,v) && validV(g,w));
	if (g->edges[v][w] == 0) {
		g->edges[v][w] = wt;
		g->edges[w][v] = wt;
		g->nE++;
	}
}

// remove an Edge
// - unsets (v,w) and (w,v)
void removeEdge(Graph g, Vertex v, Vertex w)
{
	assert(g != NULL && validV(g,v) && validV(g,w));
	if (g->edges[v][w] != 0) {
		g->edges[v][w] = 0;
		g->edges[w][v] = 0;
		g->nE--;
	}
}

// create an empty graph
Graph newGraph(int nV)
{
	assert(nV > 0);
	int **e = malloc(nV*sizeof(int*));
    /* difference between 
    int a [5]; //this is on the stack
    int a = malloc(sizeof(int) * 5); //this is on the frame, also dynamically sized
    int a[5][5]; //an array of an array of integers
    int **m = malloc(sizeof(int*)*5
    */
	assert(e != NULL);
	int i, j;
	for (i = 0; i < nV; i++) {
	   e[i] = malloc(nV * sizeof(int));
	   assert(e[i] != NULL);
	   for (j = 0; j < nV; j++){
	      e[i][j] = 0;
	   }
	}
	Graph g = malloc(sizeof(GraphRep));
	assert(g != NULL);
	g->nV = nV;
	g->nE = 0;
	g->edges = e;

	return g; 
}

// free memory associated with graph
void dropGraph(Graph g)
{
	assert(g != NULL);
	// not needed for this lab
}

// display graph, using names for vertices
void showGraph(Graph g, char **names)
{
	assert(g != NULL);
	printf("#vertices=%d, #edges=%d\n\n",g->nV,g->nE);
	int v, w;
	for (v = 0; v < g->nV; v++) {
		printf("%d %s\n",v,names[v]);
		for (w = 0; w < g->nV; w++) {
			if (g->edges[v][w]) {
				printf("\t%s (%d)\n",names[w],g->edges[v][w]);
			}
		}
		printf("\n");
	}
}

// find a path between two vertices using breadth-first traversal
// only allow edges whose weight is less than "max"
int findPath(Graph g, Vertex src, Vertex dest, int max, int *path) {	
	
	// valid graph check
	assert(g != NULL && validV(g, src) && validV(g, dest));
	int i, order = 0;

	// pre stores the order in which vertices are visited
    int *pre = malloc(g->nV*sizeof(int));

    // st stores the ancestor of each vertex visited 
    int *st = malloc(g->nV*sizeof(int));
    
    // initiailise all values of pre and st to -1
    for (i = 0; i < g->nV; i++) { 

    	pre[i] = st[i] = -1;

    }

    // create first edge between source and itself
    Edge e = mkEdge(g, src, src);
    
    //join it to the queue
    Queue q = newQueue();
    QueueJoin(q,e); pre[e.w] = order++;

    //commence BFS for destination vertex
    while (!QueueIsEmpty(q)) {
        e = QueueLeave(q);
        st[e.w] = e.v;
        int v;
        for (v = 0; v < g->nV; v++) {
            if (!g->edges[e.w][v] || g->edges[e.w][v] > max){
            	continue;
            }
            if (pre[v] == -1) {
                QueueJoin(q, mkEdge(g,e.w,v));
                pre[v] = order++;
            }
        }
    }

	// add path in reverse onto a temp path array
	int curr = dest;
	int k = 0;
	while (curr != src){
		path[k] = curr;
		curr = st[curr];
		k++;
	}

	path[k] = src;
	k++;
	
	int numberVertices = k;
	

	// reverse array 
	int temp, start, end;
	start = 0;
	end = numberVertices-1;

	while(start < end) {

		temp = path[start];
		path[start] = path[end];
		path[end] = temp;
		start++;
		end--;

	}

	// return the number of vertices stored in the path array
	return numberVertices;

}
