// GameView.c ... GameView ADT implementation

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "Globals.h"
#include "Game.h"
#include "GameView.h"
#include "Places.h"
#include "Map.h"

static int getPrevTurn (int stepsBack, int turnNum, PlayerID player);
static void writeTrail(int index, int turnNum, char *pastPlays, LocationID trail[TRAIL_SIZE]);

// global vars
static Map demoMap = NULL;
// static int prevTurn = -1;        // for use with newGameView optimisation


typedef struct _playerData {
    int health;
    int trailSize;      // should be <= TRAIL_SIZE
    LocationID location[TRAIL_SIZE]; // [0] being most recent, [T_S] being the oldest
} *PlayerData;
     
struct gameView {
    int turnNum;
    Round round;
    int score;
    char *pastPlays;
    PlayerData playerData[NUM_PLAYERS];
};

    
// Creates a new GameView to summarise the current state of the game
GameView newGameView(char *pastPlays, PlayerMessage messages[])
{
    if (demoMap == NULL) demoMap = newMap();
    GameView gameView = malloc(sizeof(struct gameView));
    gameView->turnNum = 0;
    
    int i;
    for (i = 0; i<NUM_PLAYERS; i++){
        gameView->playerData[i] = malloc(sizeof(struct _playerData));
        if (i != PLAYER_DRACULA) 
            gameView->playerData[i]->health = GAME_START_HUNTER_LIFE_POINTS;
        else if (i == PLAYER_DRACULA) 
            gameView->playerData[i]->health = GAME_START_BLOOD_POINTS;
    }
    
    int j = 0; i = 0;
    char currLocation_S[2];
    PlayerID currPlayer;
    Round currRound = 0; 
    int currTurn = 0, currScore = GAME_START_SCORE, currLocation = UNKNOWN_LOCATION;

    while (pastPlays[i+1] != '\0') {    // this will take 5ever
        // traverse string. 
        if (j == 0) {
            switch (pastPlays[i]) {
            case 'G': currPlayer = PLAYER_LORD_GODALMING; break;
            case 'S': currPlayer = PLAYER_DR_SEWARD; break;
            case 'H': currPlayer = PLAYER_VAN_HELSING; break;
            case 'M': currPlayer = PLAYER_MINA_HARKER; break;
            case 'D': currPlayer = PLAYER_DRACULA; break;
            default: fprintf(stderr, "pastPlays traversal error\n"); break;
            }
            currTurn++;
        }
        else if (j == 1) {
            // parse in current location
            currLocation_S[0] = pastPlays[i++];
            currLocation_S[1] = pastPlays[i];
            currLocation = abbrevToID(currLocation_S);
        }
        else if (j >= 3 && j <= 7) {
            // parse in actions
            // calculate score
            // calculate player health
        }        
        else if (j == 8) {
            j = 0;
            currRound++;
            assert(pastPlays[i] == ' ');
        }
        
        /*
        if(gameView->playerData[currPlayer]->trailSize == TRAIL_SIZE) {
            // do weird append/drop magic to the trail 
            // such that ->location[0] is the most recent
            //  and ->location[TRAIL_SIZE] is the least recent
        } else {
            int trailSize = gameView->playerData[currPlayer]->trailSize;
            gameView->playerData[currPlayer]->location[trailSize] = currLocation;
        }
        */
        
        i++;
    }
    
    
    // update gameview. do not rely on var i
    gameView->turnNum = currTurn;
    gameView->round = currRound;
    gameView->score = currScore;
    
    gameView->playerData[currPlayer]->location[0] = currLocation;
    
    gameView->pastPlays = pastPlays;
    return gameView;
}
    
    
// Frees all memory previously allocated for the GameView toBeDeleted
void disposeGameView(GameView toBeDeleted)
{
    free(toBeDeleted);
}


//// Functions to return simple information about the current state of the game

// Get the current round
Round getRound(GameView currentView)
{
    return ((currentView->turnNum)/5);
}

// Get the id of current player - ie whose turn is it?
PlayerID getCurrentPlayer(GameView currentView)
{
    int turnNum = currentView->turnNum;
    int playerID = 0;

    while (turnNum%5 != 0 && count < NUM_PLAYERS) {
        playerID++;
    }

    //REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    return playerID;
}

// Get the current score
int getScore(GameView currentView)
{
    return (currentView->score);
}

// Get the current health points for a given player
int getHealth(GameView currentView, PlayerID player)
{
    return (currentView->playerData[player]->health);
}

// Get the current location id of a given player
LocationID getLocation(GameView currentView, PlayerID player)
{
    return (currentView->playerData[player]->location[0]);
}


//// Functions that return information about the history of the game

// Fills the trail array with the location ids of the last 6 turns
void getHistory(GameView currentView, PlayerID player,
                            LocationID trail[TRAIL_SIZE])
{
    int turnNum = currentView->turnNum;
    int counter = 0;
    while (counter < 6){
        writeTrail(counter, getPrevTurn(counter, turnNum, player), currentView->pastPlays, trail);
        counter++;
    }

}


//// Functions that query the map to find information about connectivity

// Returns an array of LocationIDs for all directly connected locations
// int road, rail, sea are boolean params

LocationID *connectedLocations(GameView currentView, int *numLocations,
                               LocationID from, PlayerID player, Round round,
                               int road, int rail, int sea)
{
    assert(numLocations != NULL && demoMap != NULL);
    assert(validPlace(from));
    assert(player <= NUM_PLAYERS && player >= 0);
    if (from != currentView->playerData[player]->location[0]) {
        fprintf(stderr, " warning: currentView is out of date\n");
    };
    
    LocationID *connectedLocs = malloc(sizeof(LocationID) * NUM_MAP_LOCATIONS);
    int counter = 0;
    connectedLocs[0] = from;
    
    // search in demoMap for connected nodes. 
    VList curr = demoMap->connections[from];
    while (curr != NULL) {
        // exceptions
        if (curr->v == ST_JOSEPH_AND_ST_MARYS && player == PLAYER_DRACULA) 
            curr = curr->next;
    
        if (curr->type == ROAD && road) 
            connectedLocs[++counter] = curr->v;
        else if (curr->type == SEA && sea)
            connectedLocs[++counter] = curr->v;
        else if (curr->type == RAIL && rail && player != PLAYER_DRACULA) 
            connectedLocs[++counter] = curr->v;

        curr = curr->next;
    }
    // assert(connectedLocs[counter+1] = '\0');
    
    counter = numLocations[0];
    return connectedLocs;
}




//Helper functions
static int getPrevTurn (int stepsBack, int turnNum, PlayerID player){
    int counter = 0;

    while (counter != stepsBack){
        while((turnNum%5) != player){
            turnNum--;
        }
        counter++;
    }

    return turnNum;
}


static void writeTrail(int index, int turnNum, char *pastPlays, LocationID trail[TRAIL_SIZE]){
    
    char Location[2];
    Location[0] = pastPlays[(turnNum*8) + 1];
    Location[1] = pastPlays[(turnNum*8) + 2];
    int i = 0;

    while (strcmp(idToName(i), Location) != 0) {
        i++;
    }

    trail[index] = i;
}