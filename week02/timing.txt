
Input Size	Initial Order	Numberof runs	Avg Timeor uil	Avg Timefor sort

10000 		random			3 				0.15s				0.01s
			
			sorted			3				0.2s 				0.01s

			reverse 		3				0.15s				0.01s


50000       random			3 				6.59s				0.08s
				
			sorted			3               5.43s				0.06s

			reverse			3				0.02s				0.07s


100000		random			3 				30.9s				0.19s
				
			sorted			3               20.24s				0.15s
 
			reverse			3			    0.2s    			0.16s				


Note: insertInOrder on a reversed list takes n time while  (as insertion is O(1))
      insertInOrder on a random or already sorted list takes n^2 time